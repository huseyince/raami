$("#menu_haber_ekle").on("click", function() {
    $("#contacts").hide(200, function() {
        $("#haber_ekle_form").toggle(200);
    });
});

$("#menu_contacts_message").on("click", function() {
    $("#haber_ekle_form").hide(200, function(){
        $("#contacts").toggle(200);
    });
});

function delete_contacts(id) {
    var temp_id = id.substring(16);
    $.get({
        url: "http://localhost:3000/contacts/delete/"+temp_id,
        success: $("#"+id).parent().parent().fadeOut(),
    });
}

$('.carousel').carousel({
    interval: 3000
  })