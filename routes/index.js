var express = require('express');
var mysql = require('mysql');
var md5 = require('md5');


var con = mysql.createConnection({
  host: "localhost",
  database: "raami",
  user: "raami",
  password: "raami"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
});

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.session.username == undefined) {
    req.session.username = "";
  }
  if (req.session.authorised == undefined) {
    req.session.authorised = 0;
  }
  if (req.session.is_admin == undefined) {
    req.session.is_admin = 0;
  }

  con.connect(function(err) {
    sql = "SELECT * FROM events LIMIT 9";
    con.query(sql, function (err, result, fields) {
      res.render('index', {
        title: 'Index',
        session: req.session,
        news: result,
      });
    });
  });
});

router.get('/about', function(req, res, next) {
  res.render('about', { title: 'About', session: req.session});
});

router.get('/reference', function(req, res, next) {
  res.render('reference', { title: 'Reference', session: req.session});
});

router.get('/contact', function(req, res, next) {
  res.render('contact', {
    title: 'Contact',
    session: req.session,
    message: "",
  });
});

router.post('/contact', function(req, res, next) {
  if (req.body.name != "" && req.body.surname != "" && req.body.email != "" && req.body.message != "") {
    name = req.body.name
    surname = req.body.surname
    email = req.body.email
    message = req.body.message
  } else {
    res.render('contact', {
      title: 'Contact',
      session: req.session,
      message: "Alanları boş bırakmayınız.",
      status: "bad",
    });
    return;
  }

  con.connect(function(err) {
    sql = 'INSERT INTO contacts VALUES (NULL, "'+name+'", "'+surname+'", "'+email+'", "'+message+'");';
    con.query(sql, function (err, result, fields) {
      if (err) {
        res.render('contact', {
          title: 'Contact',
          session: req.session,
          message: "Geçici bir sebepten dolayı hizmet veremiyoruz.",
          status: "bad",
        });
      }
      res.render('contact', {
        title: 'Contact',
        session: req.session,
        message: "Mesajınız başarılı bir şekilde tarafımıza ulaşmıştır.",
        status: "good",
      });
    })
  });

  res.render('contact', {title: 'Contact', session: req.session});
});

router.post('/event/add', function(req, res, next) {
  title = req.body.title;
  content = req.body.content;
  image = req.files.image;
  image_ext = image.name.substring(image.name.lastIndexOf("."));
  file_name = image.md5+image_ext;
  image.mv(__dirname.substring(0,__dirname.lastIndexOf("/"))+"/public/images/events/"+file_name);
  con.connect(function(err) {
    sql = 'INSERT INTO events VALUES (NULL, "'+title+'", "'+content+'", "'+file_name+'");';
    con.query(sql, function (err, result, fields) {
      if (err) {
        console.log(sql);
      }
    })
  });

  res.render('admin', { title: 'Admin', session: req.session});
});

router.get('/login', function(req, res, next) {
  res.render('login', {title: 'Login', session: req.session});
});

router.get('/logout', function(req, res, next) {
  req.session.username = "";
  req.session.authorised = 0;
  req.session.is_admin = 0;
  res.render('login', {
    title: 'Login',
    session: req.session,
    message: "Çıkış Başarılı",
    status: "good",
  });
});

router.get('/admin', function(req, res, next) {
  con.connect(function(err) {
    sql = "SELECT * FROM contacts ORDER BY id DESC";
    con.query(sql, function (err, result, fields) {
      if (err) {
        res.render('error', {title: 'Admin', session: req.session, status:300});
      }
      res.render('admin', {
        title: 'Admin',
        session: req.session,
        contacts: result,
      });
    });
  });
});

router.get('/contacts/delete/:id', function(req, res, next) {
  con.connect(function(err) {
    sql = "DELETE FROM contacts WHERE id = "+req.params.id;
    con.query(sql, function (err, result, fields) {
      console.log("Mesajı Silme Başarılı");
    });
  });
});

router.post('/login', function(req, res, next) {
  if (req.body.username != "" && req.body.password != "") {
    con.connect(function(err) {
      sql = "SELECT * FROM users where username='"+req.body.username+"'";
      con.query(sql, function (err, result, fields) {
        if (result[0] != undefined) {
          if(md5(req.body.password) == result[0].password) {
            req.session.username = req.body.username;
            req.session.authorised = 1;
            if (result[0].is_admin == 1) {
              req.session.is_admin = 1;
              res.render('about', {title: 'About', session: req.session});
            }
            res.render('about', {title: 'About', session: req.session});
          } else {
            res.render('login', {
              title: 'Login', 
              session: req.session,
              message: 'Kullanıcı Adı veya Parola yanlış!',
              status: "bad",
            });
          }
        } else {
          res.render('login', {
            title: 'Login', 
            session: req.session,
            message: 'Kullanıcı Adı veya Parola yanlış!',
            status: "bad",
          });
        }
      });
    });
  } else {
    res.render('login', {
      title: 'Login',
      session: req.session,
      message: 'Kullanıcı Adı veya Parola boş olamaz!',
      status: "bad",
    });
  }
});

module.exports = router;
